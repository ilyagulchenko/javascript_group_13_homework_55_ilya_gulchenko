import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { IngridientsComponent } from './ingridients/ingridients.component';
import { IngridientItemComponent } from './ingridients/ingridient-item/ingridient-item.component';
import { BurgerComponent } from './burger/burger.component';
import { BurgerItemComponent } from './burger/burger-item/burger-item.component';

@NgModule({
  declarations: [
    AppComponent,
    IngridientsComponent,
    IngridientItemComponent,
    BurgerComponent,
    BurgerItemComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
