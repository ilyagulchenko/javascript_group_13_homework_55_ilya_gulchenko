import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-ingridient-item',
  templateUrl: './ingridient-item.component.html',
  styleUrls: ['./ingridient-item.component.css']
})
export class IngridientItemComponent {
  meatCount: number = 0;
  cheeseCount: number = 0;
  saladCount: number = 0;
  baconCount: number = 0;

  changeMeatCount() {
    this.meatCount += 1;
  }

  changeCheeseCount() {
    this.cheeseCount += 1;
  }

  changeSaladCount() {
    this.saladCount += 1;
  }

  changeBaconCount() {
    this.baconCount += 1;
  }

  deleteMeatCount() {
    this.meatCount -= 1;
  }

  deleteCheeseCount() {
    this.cheeseCount -= 1;
  }

  deleteSaladCount() {
    this.saladCount -= 1;
  }

  deleteBaconCount() {
    this.baconCount -= 1;
  }
}
